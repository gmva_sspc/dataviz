# Impact environmental

## Grafana

Grafana est un logiciel open source de visualisation et de monitoring de données. Étant donné que Grafana est un logiciel, son impact environnemental est principalement lié à l'utilisation de l'ordinateur et des ressources nécessaires pour exécuter et stocker les données collectées.

L'impact environnemental de Grafana dépendra de la quantité de données collectées et stockées, ainsi que de la puissance de traitement nécessaire pour exécuter le logiciel. Plus la quantité de données est importante et plus le système est puissant, plus la consommation d'énergie et les émissions de gaz à effet de serre seront élevées.

Grafana est impliqué dans son impact environmental et mets en place des moyens pour le développement durable des sociétés. Pour en savoir plus sur les actions entreprises par Grafana : 

How activist engineers use Grafana Cloud to improve global air quality : 
https://grafana.com/blog/2022/08/02/how-activist-engineers-use-grafana-cloud-to-improve-global-air-quality/

How Grafana helps Green City Solutions combat air pollution : 
https://grafana.com/blog/2022/10/04/visualizing-an-invisible-problem-how-grafana-helps-green-city-solutions-combat-air-pollution/

How New City Energy is supporting sustainability with Grafana : 
https://grafana.com/blog/2019/03/14/how-new-city-energy-is-supporting-sustainability-with-grafana/
