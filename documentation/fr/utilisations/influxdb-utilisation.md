Guide d'utilisation d'InfluxDB
==============================

Dans ce guide, nous allons vous montrer comment utiliser le logiciel **InfluxDB** pour créer des graphiques, les modifier, etc...

### Créer un graphique

> 1 - D'abord sur InfluxDB, il faut cliquer sur l'icone avec un graphe : **Data explorer** :

> 2 - Ensuite il faut séléctionner dans **From** une base de données. Pour cette exemple on utilisera la base de données qui regroupe les données météos **OWM**. 
> > ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/influxdb-utilisation/guide-utilisation.png "buckets")

> 3. Nous pouvons alors filtrer les données qui nous intéréssent, par exemple on souhaite montrer les température dans les villes d'Aradon, Arzon et Baden les 30 derniers jours, nous allons ainsi cocher les cases correpondantes 
> > ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/influxdb-utilisation/guide-utilisation2.png "buckets")

> 4. Il suffit ensuite de cliquer sur **SUBMIT** pour avoir un graphe.
> > ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/influxdb-utilisation/guide-utilisation3.png "buckets")

> 5 - Pour utiliser les données sur **Graphana** (car en effet nous n'avons que peu de choix de graphes sur InfluxDB) il faut d'abord cliquer sur **SCRIPT EDITOR** et copier le code. 
> > ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/influxdb-utilisation/guide-utilisation4.png "buckets")
