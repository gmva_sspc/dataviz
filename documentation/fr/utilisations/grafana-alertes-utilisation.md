Guide d'utilisation des alertes sur Grafana
================================================

### I. Création d'une alerte

> 1. Il faut d'abord aller sur "Alerting" et cliquer sur "New alert rule"
> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/alert/alert1.png)
>
>
> 2. Ensuite, indiquer votre requête influxDB
>
> 3. Cliquer sur "Sample Query" pour visualiser la requête, et vérifier qu'elle est bonne
> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/alert/alert2.png)
>
> 4. Ensuite configurer l'alerte
>> ### Pour faire une alerte basé sur un seuil
>> 1. Dans opération sélectioner "Simple condition"
>> 2. Indiquer quel condition doit être remplis pour que l'alerte se lance
>>
>> | Champ                                       | Valeurs                       | Description                                                                                                                                                                                                                   |
>> |---------------------------------------------|-------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
>> | WHEN                                        | avg(), min(), max(), sum()... | Ce champ défini qu'elle valeur de la querry faut il tester, par exemple si last() est selectionner alors la condition prenda la dernière valeur de la querry, avce avg() ce sera la moyenne des valeurs sur le temps donné... |
>> | OF                                          | A                             | Ce champ défini la source des données, A équivaut à la querry remplit                                                                                                                                                         |
>> | IS ABOVE, IS BELOW, IS OUTSIDE THE RANGE... | NUMBER                        | Ce champ correspond à la condition qui sera tester, si le champ sélectionner est IS ABOVE et que la valeur est 3 alors la condition testera si la valeur extraite de la querry est supérieur à 3                              |
>> 
>> Dessous nous avons configurer des alertes lorsque la température max dépasse 13°C et lorsque la température minimal est en dessous de 0°C
>> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/alert/alert3.png)
>>
>> 3. Choisir l'interval de temps entre chaque test de la requête et le délai avant que la condition déclanche une alerte.
>> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/alert/alert4.png)
>> 4. Puis, ajouter des détails sur l'alerte tel que son nom, dans quel dossier elle apparaît, une description etc...
>> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/alert/alert5.png)
>> 5. Ensuite, ajouter à votre notification une étiquette qui indique par exemple la sévérité de l'alarme afin d'organiser et de classer les alarmes
>> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/alert/alert6.png)
> 5. Enfin, appuyer sur "Save"
> 6. Vous pouvez ainsi voir votre alerte en allant dans son dossier. À la fin du temps impartit, le statue de l'alarme sera affiché.
> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/alert/alert7.png)
