Guide d'utilisation de Grafana
==============================

Dans ce guide, nous allons vous montrer comment utiliser le logiciel **Grafana** pour créer des graphiques, les modifier, etc...

### Créer un graphe

> 1 - D'abord sur **Grafana**, il faut se rendre dans **Dashboard**, **new dashboard**
> > ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-utilisation/guide-utilisation5.png "buckets")

> 2 - Dans un premier temps, créer un nouveau panel en cliquant sur **add a new panel** 
> > ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-utilisation/guide-utilisation6.png "buckets")

> 3 - Dans un second temps, séléctionner InfluxDB dans **data source** et coller les queries vue dans InfluxDB. Filtrer le temps pour choisir la page horaire des données température, et cliquer sur **Apply** . Vous devriez ainsi avoir un graphe par défaut qui s'affiche.
> > ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-utilisation/guide-utilisation7.png "buckets")

> 4 - Pour choisir un autre type de graphe cliquer sur **Times Series** et choisissez dans la liste déroulante le type de graphe que vous souhaiter.
> > ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-utilisation/guide-utilisation8.png "buckets")

> Voici deux types de graphes :
> > ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-utilisation/guide-utilisation9.png "buckets")
> > ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-utilisation/guide-utilisation10.png "buckets")
