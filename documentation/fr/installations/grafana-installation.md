Guide d'installation de Grafana sur Docker
==========================================

Dans ce guide, nous allons vous guider sur l'installation du logiciel **Grafana**, que nous utiliserons dans le cadre de notre projet pour le **Golfe du Morbihan Vannes Agglomération** (GMVA).

**Grafana** nous permettra d'exploiter les données stockés de la base InfluxDB en les visualisants avec des graphiques, des tableaux, et des indicateurs pertinents afin d'améliorer le suivi des installations communautaires par **GMVA**.

Windows
---

### I. Installation de Grafana
> 1. Dans un terminal executez la commande :
> ```shell
> docker run -d -p 3000:3000 grafana/grafana-oss:9.2.5-ubuntu
> ```
> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-installation/grafana-installation1.png)

### II. Configuration de Grafana
> 1. Se rendre sur votre navigueur préféré puis taper [localhost:3000](https://localhost:3000).
> 2. Sur la page de connexion entrez :
> **Username :** ``admin`` 
> **Password :** ``admin``

### III. Liaison d'InfluxDB à Grafana
> Pour pouvoir faire des graphes avec les donnée d'Influx il va falloir lié les deux, pour cela nous aurrons besoin d'un Token d'accès à la base InfluxDB celui-ci nous donnera les permissions d'acceder ou non aux différentes données
>> ###### Création d'un token d'accès sur InfluxDB pour Grafana
>> 1. Rendez vous sur [InfluxDB](http://localhost:8086/)
>> 2. Allez sur l'onglet **"API Token"** (2ème icone dans le menu latéral gauche)
>> 3. Puis sur le bouton **"+ Generate API Token"** et selectionner **"All access token"**
>>
>> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-installation/grafana-installation2.png)
>>
>> 4. Copiez le Token (ne le perdez surtout pas il n'est pas possible de le voir à nouveau, il faudra le regénérer)
>>
>> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-installation/grafana-installation3.png)
>>
> 5. Rendez vous sur **"Configuration"**
> 6. Puis sur **"Data source"**
>
> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-installation/grafana-installation4.png)
>
> 7. Et cliquez sur **"Add data source"**
>
> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-installation/grafana-installation5.png)
>
> 8. Parmis les propositions dans **"Times series databases"**, ajouter *InfluxDB* comme source de données
>
> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-installation/grafana-installation6.png)
>
> 9. Dans la configuration, choisir **"Flux"** comme **"Query Langages"** puis remplir les différent champs
> 10. Pour lié notre base *InfluxDB* hébergé sur *Docker*, dans la partie HTTP, inserrez cette URL ```http://host.docker.internal:8086```
>
> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-installation/grafana-installation7.png)
