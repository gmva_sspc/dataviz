InfluxDB Usage Guide
==============================

In this guide, we will show you how to use the **InfluxDB** software to create and modify graphs.

### Creating a graph

> 1 - First, in InfluxDB, click on the graph icon: **Data explorer**:

> 2 - Next, select a database in **From**. For this example, we will use the weather data database **OWM**. 
> > ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/influxdb-utilisation/guide-utilisation.png "buckets")

> 3. We can then filter the data that interests us, for example, we want to show the temperatures in the cities of Aradon, Arzon, and Baden for the last 30 days, so we will check the corresponding boxes 
> > ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/influxdb-utilisation/guide-utilisation2.png "buckets")

> 4. Then simply click on **SUBMIT** to have a graph.
> > ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/influxdb-utilisation/guide-utilisation3.png "buckets")

> 5 - To use the data on **Grafana** (as we have very few graph options in InfluxDB) you first have to click on **SCRIPT EDITOR** and copy the code. 
> > ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/influxdb-utilisation/guide-utilisation4.png "buckets")
