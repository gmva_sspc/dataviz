Installation Guide for InfluxDB on Docker
=========================================

In this guide, we will guide you through the installation of the software **InfluxDB**, which we will use as part of our project for the **Gulf of Morbihan Vannes Agglomération** (GMVA).

**InfluxDB** will allow us to store data received from different sensors, in order to later exploit it with relevant tables, graphs, and indicators to improve the monitoring of community installations by **GMVA**.

On Windows
---

### I. Docker installation verification
> 1. To check that *Docker* is properly installed, run the command:
> ```shell
> docker -version
> ```
> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/influxdb-installation/influxdb-installation1.png "Main Page")

### II. InfluxDB installation
> 1. Run the following command in a terminal:
> ```shell
> docker pull influxdb
> ```
> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/influxdb-installation/influxdb-installation2.png "Command execution")
![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/influxdb-installation/influxdb-installation3.png "Result obtained")

### III. Launching InfluxDB
> 1. Run the command:  
> ```shell
> docker run --name influxdb -p 8086:8086 quay.io/influxdb/influxdb:2.0.0-rc
> ```
> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/influxdb-installation/influxdb-installation4.png "If no InfluxDB containers exist, a new one will be created")

### IV. InfluxDB Configuration
> 1. Go to http://localhost:8086/
> 2. Click on **"get started"**
> 3. Fill in the username, password, confirm password, give an organization name, and a bucket name fields
>
> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/influxdb-installation/influxdb-installation5.png "Configuration page")
> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/influxdb-installation/influxdb-installation6.png "Configuration page filled in")

### V. Next Steps
> [Continue with Grafana installation](https://gitlab.com/gmva_sspc/dataviz/-/blob/main/Documentation%20/grafana.md)
