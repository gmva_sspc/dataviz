Docker Installation Guide
=========================

In this guide, we will guide you through the installation of **Docker**, which we will use in the context of our project for the **Gulf of Morbihan Vannes Agglomeration** (GMVA).

**Docker** will allow us to host our two tools **InfluxDB** and **Grafana** so that we can work together throughout the project.

On Windows
---

### I. Docker Download 
> 1. Go to the [Docker](https://www.docker.com/products/docker-desktop/) website
> 2. Click on the download

### II. Docker Installation 
> 1. Launch the installer
> 2. After loading, you will be asked to **"log out"**
> 3. Then, click on Docker Desktop. Accept the terms of use.
> 4. Click on **"Start"**

### III. Next steps
> [Continue with the Grafana installation](https://gitlab.com/gmva_sspc/dataviz/-/blob/main/Documentation%20/influxdb.md)
