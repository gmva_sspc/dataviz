Grafana Installation Guide on Docker
=====================================

In this guide, we will guide you on the installation of the **Grafana** software, which we will use as part of our project for the **Gulf of Morbihan Vannes Agglomeration** (GMVA).

**Grafana** will allow us to use the stored data from the InfluxDB database by visualizing it with graphs, tables, and relevant indicators to improve the monitoring of community installations by **GMVA**.

Windows
---

### I. Grafana Installation
> 1. In a terminal, run the following command:
> ```shell
> docker run -d -p 3000:3000 grafana/grafana-oss:9.2.5-ubuntu
> ```
> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-installation/grafana-installation1.png)

### II. Grafana Configuration
> 1. Go to your preferred browser and type [localhost:3000](https://localhost:3000).
> 2. On the login page, enter:
> **Username :** ``admin`` 
> **Password :** ``admin``

### III. InfluxDB Link to Grafana
> In order to create graphs with Influx data, it will be necessary to link the two, for this we will need an InfluxDB access Token that will give us permissions to access or not the different data
>> ###### Creation of an InfluxDB access token for Grafana
>> 1. Go to [InfluxDB](http://localhost:8086/)
>> 2. Go to the **"API Token"** tab (2nd icon in the left side menu)
>> 3. Then press the **"+ Generate API Token"** button and select **"All access token"**
>>
>> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-installation/grafana-installation2.png)
>>
>> 4. Copy the Token (do not lose it, it is not possible to see it again, it will have to be regenerated)
>>
>> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-installation/grafana-installation3.png)
>>
> 5. Go to **"Configuration"**
> 6. Then to **"Data source"**
>
> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-installation/grafana-installation4.png)
>
> 7. And click on **"Add data source"**
>
> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-installation/grafana-installation5.png)
>
> 8. Among the proposals in **"Times series databases"**, add *InfluxDB* as a data source
>
> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-installation/grafana-installation6.png)
>
> 9. In the configuration, choose **"Flux"** as **"Query Languages"** and fill in the various fields
> 10. To link our *InfluxDB* database hosted on *Docker*, in the HTTP section, insert this URL ```http://host.docker.internal:8086```
>
> ![](https://gitlab.com/gmva_sspc/dataviz/-/raw/main/documentation/img/grafana-installation/grafana-installation7.png)
