Les auteurs de ce projet pour la visualisation des données
==========================================================


## Développeurs

* __[Peter Michon](https://gitlab.com/petermichon44) (Scrum Master)__

    Temps passé sur le projet : ```186 heures et 10 minutes```

* __[Tobias Dumoulin](https://gitlab.com/tobias.d)__

    Temps passé sur le projet : ```245 heures et 15 minutes```

* __[Chadène Kerbous](https://gitlab.com/Chadene)__

    Temps passé sur le projet : ```177 heures et 0 minutes```

* __[Romain Quéré](https://gitlab.com/romainqre)__

    Temps passé sur le projet : ```185 heures et 40 minutes```


## Tuteur

* __[Minh-Tan Pham](https://gitlab.com/mtpham)__


## Responsable du projet

* __[Nicolas Le Sommer](https://gitlab.com/mtpham)__


## Contributeurs du projet

* __[Application Web](https://gitlab.com/gmva_sspc/web_app)__
* __[Prévision Météorologiques](https://gitlab.com/gmva_sspc/meteo_prev)__
* __[Visualisation des données](https://gitlab.com/gmva_sspc/dataviz)__
