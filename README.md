# Sommaire

1. [Introduction](#introduction)
2. [Organisation globale du projet](#organisation-globale-du-projet)
3. [Organisation détaillée du projet](#organisation-détaillée-du-projet)
4. [Vidéo de présentation du projet](#vidéo-de-présentation-du-projet)
5. **[SSPC Deployment Tool](#/!\-sspc-deployment-tool-/!\)**
6. [Autres documentations](#autres-documentations)

# Introduction

Le patrimoine de Golfe du Morbihan Vannes Agglomération (GMVA) a
évolué ces dernières années à la fois en quantité mais aussi en
technicité. Le suivi et l’amélioration des performances énergétiques des
installations communautaires doit permettre de réduire les
consommations énergétiques et ainsi atteindre les objectifs collectifs
du Plan Climat Air Energie territorial, notamment de diminuer de 30%
les consommations du territoire à horizon 2030 par rapport à 2012.

L’objectif du projet proposé est d’analyser les données de suivi des
installations électriques, chauffage, ventilation, climatisation en
vue de l’amélioration de l’efficacité énergétique du patrimoine et le
suivi des installations de production d’énergie renouvelable de la
collectivité et ainsi proposer les modules de régulation et des
systèmes d’alerte pouvant être fonction, par exemple de données de
prévisions météorologiques, etc.

Dans ce projet, il s'agit de développer la partie consacrée à
visualisation des données des installations électriques, chauffage,
ventilation et climatisation. Les données devront être présentées de
manière simple pour que celles-ci puissent être facilement
comprises. Les données pourront être présentées via des graphiques et
des tableaux. Les dernières données collectées et un historique des
données devront être présentés. L'uniformisation des données
collectées et leur stockage en base de données sont réalisés dans le
projet <https://gitlab.com/gmva_sspc/datasrc>.

# Organisation globale du projet

Ce schéma représente l'organisation globale du projet. On peut y voir les différentes étapes de la gestion de la donnée :

- Récolte
- Insertion
- Stockage
- Visualisation

En entrée de ce circuit, on a deux sources de données : les données météorologiques provenant de sites web, et les données énergétiques du client. En sortie, on a des graphiques qui représentent ces données. Ces graphiques sont implémentés dans une application web qui sera livrée au client.

![](documentation/diagrammes/diagramme-global.png)

# Organisation détaillée du projet

Pour une représentation plus détaillée de l'organisation technique du projet, vous pouvez vous référer au schéma suivant.

![](documentation/diagrammes/diagramme-architecture.png)

# Vidéo de présentation du projet

Cette vidéo présente un prototype de l'application web ainsi qu'un aperçu de la gestion des graphiques.

![](/documentation/Vidéo_de_démonstration_SSPC.mp4)

# /!\ SSPC Deployment Tool /!\
CE SCRIPT PERMET DE DEPLOYER LES PROJETS SSPC SUR DOCKER.
CETTE INSTALLATION EST DONC À FAIRE UNE SEULE FOIS SUR 1 DES 4 PROJETS.

Ce projet permet de déployer les projets SSPC sur docker.

Il est composé de 4 scripts bash permettant de télécharger, de construire et de lancer les conteneurs docker, ainsi qu'un fichier docker-compose afin de configurer les différents conteneurs.


## Partie Installation 
Il faut préalablement installer docker et docker-compose sur votre machine.

Pour tout faire il faut exécuter le fichier `all.sh` qui va d’abord télécharger les repos avec `download.sh` puis construire les images des projets avec `build.sh` et enfin lancer les conteneurs avec le docker-compose grâce à `run.sh`.

Donc pour commencer il faut exécuter la commande suivante dans le dossier du projet :

`sudo sh ./all.sh`

## Configuration

Il faut configurer InfluxDB comme expliquer dans la documentation du projet (le pdf).

Il faut aussi configurer un compte Gmail pour les rapport de chauffage. (Voir le tutoriel du groupe chauffage)

Une fois la configuration d'InfluxDB faite, vous devez avoir un nom d'organisation, un nom de bucket ainsi qu'un AllAccessToken que vous avez créer.

Il faut ensuite modifier le fichier `docker-compose.yml` et remplacer les valeurs des variables d'environnement du projet chauffage par les valeurs que vous avez récupérer.

Voici les valeurs à remplacer :
```
environment:
    - MAIL=YOUR MAIL HERE !!!
    - MAIL_SENDER=YOUR MAIL SENDER HERE !!!
    - MAIL_PASSWORD=YOUR MAIL PASSWORD HERE !!!
    - TOKEN_INFLUXDB=YOUR TOKEN HERE !!!
    - ORGANIZATION_INFLUXDB=YOUR ORGANIZATION HERE !!!
```

Après les avoir remplacer, il faut ré-exécuter le fichier all.sh pour prendre en compte les modifications.

Après ça il reste quelques configuration à faire sur grafana pour le groupe de visualisation ainsi que la configuration des tokens du projet méteo_prev.

## Annexe

Voici les adresse web des différents projets après le déploiement :

- [Groupe Web : http://localhost:80](http://localhost:80)
- [Meteo Prev : http://localhost:83](http://localhost:83)
- [InfluxDB : http://localhost:81](http://localhost:81)
- [Grafana : http://localhost:82](http://localhost:82)

(le localhost peut être remplacer par l'adresse IP de la machine)

# Autres documentations

## Guides d'installations

### Français

- [Guide d'installation de Docker](/documentation/fr/installations/docker-installation.md)
- [Guide d'installation de Grafana](/documentation/fr/installations/grafana-installation.md)
- [Guide d'installation de InfluxDB](/documentation/fr/installations/influxdb-installation.md)

### Anglais

- [Guide d'installation de Docker](/documentation/en/setups/docker-setup.md)
- [Guide d'installation de Grafana](/documentation/en/setups/grafana-setup.md)
- [Guide d'installation de InfluxDB](/documentation/en/setups/influxdb-setup.md)


## Guides d'utilisations

### Français

- [Guide d'utilisation des alertes de Grafana](/documentation/fr/utilisations/grafana-alertes-utilisation.md)
- [Guide d'utilisation de Grafana](/documentation/fr/utilisations/grafana-utilisation.md)
- [Guide d'utilisation des alertes d'InfluxDB](/documentation/fr/utilisations/influxdb-alertes-utilisation.md)
- [Guide d'utilisation d'InfluxDB](/documentation/fr/utilisations/influxdb-utilisation.md)

### Anglais

- [Guide d'utilisation de Grafana](/documentation/en/usages/grafana-usage.md)
- [Guide d'utilisation d'InfluxDB](/documentation/en/usages/influxdb-usage.md)
