# Code source

Cette documentation concerne le code source du projet.

#### Requêtes

Sur InfluxDB, plusieurs requêtes ont été écrites afin de générer les graphiques.

> La température moyenne à l'intérieur du bâtiment 1

```sql
from(bucket: "batiment_1")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "indoor")
  |> filter(fn: (r) => r["_field"] == "temperature")
  |> group(columns: ["_time"])
  |> group(columns: ["result"])
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

> La température extérieure

```sql
from(bucket: "OWM")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "ARRADON")
  |> filter(fn: (r) => r["_field"] == "temperature")
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

> La température et les seuils de chauffage/refroidissement du bâtiment.

```sql
from(bucket: "batiment_1")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "indoor")
  |> filter(fn: (r) => r["_field"] == "temperature" or r["_field"] == "heating" or r["_field"] == "cooling")
  |> group(columns: ["_field"])
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

> La température moyenne du bâtiment 1

```sql
from(bucket: "batiment_1")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "indoor")
  |> filter(fn: (r) => r["_field"] == "temperature")
  |> group(columns: ["_time"])
  |> group(columns: ["result"])
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

> La température extérieure

```sql
from(bucket: "OWM")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "ARRADON")
  |> filter(fn: (r) => r["_field"] == "temperature")
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

> La consommation énergétique du bâtiment 1

```sql
from(bucket: "batiment_1")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "electricite")
  |> filter(fn: (r) => r["_field"] == "Consumption")
  |> filter(fn: (r) => r["hvac"] == "hvac_1")
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

> Le débit d'air filtré par RTU

```sql
from(bucket: "batiment_1")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "RTU")
  |> filter(fn: (r) => r["_field"] == "supply_air_flow")
  |> filter(fn: (r) => r["RTU"] == "RTU_001" or r["RTU"] == "RTU_002")
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

> La moyenne des consommations énergétiques du bâtiment 1

```sql
from(bucket: "batiment_1")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "electricite")
  |> filter(fn: (r) => r["_field"] == "Consumption")
  |> filter(fn: (r) => r["hvac"] == "hvac_1")
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

> La température de chaque pièce du bâtiment 1

```sql
from(bucket: "batiment_1")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "indoor")
  |> filter(fn: (r) => r["_field"] == "temperature")
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

> L'émission moyenne de CO2 du bâtiment 1 

```sql
from(bucket: "batiment1")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "co2")
  |> filter(fn: (r) => r["_field"] == "co2")
  |> group(columns: ["_time"])
  |> group(columns: ["result"])
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

> L'émission moyenne de CO2 du bâtiment 2

```sql
from(bucket: "batiment2")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "co2")
  |> filter(fn: (r) => r["_field"] == "co2")
  |> group(columns: ["_time"])
  |> group(columns: ["result"])
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

> Température moyenne du bâtiment 1 

```sql
from(bucket: "batiment1")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "exterior")
  |> filter(fn: (r) => r["_field"] == "temperature")
  |> group(columns: ["_time"])
  |> group(columns: ["result"])
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```

> Température moyenne du bâtiment 2

```sql
from(bucket: "batiment2")
  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
  |> filter(fn: (r) => r["_measurement"] == "co2")
  |> filter(fn: (r) => r["_field"] == "co2")
  |> group(columns: ["_time"])
  |> group(columns: ["result"])
  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)
  |> yield(name: "mean")
```
